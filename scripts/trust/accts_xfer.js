var users = require('../../users').users;
function xfer_gc(to, from, amount, memo){
    setTimeout(()=>{
        genKey(24, id=>{
            var tobj = {
                id: id,
                t: ts(),
                script_name: 'accts.xfer',
                data:{
                    direction: 'from',
                    sender: from.username,
                    amount: lib.to_gc_str(amount),
                    memo: memo,
                    success: true,
                    is_notification: true
                }
            };
            tobj.data = JSON.stringify(tobj.data);
            var fobj = {
                id: id,
                t: ts(),
                script_name: 'accts.xfer',
                data:{
                    direction: 'to',
                    recipient: to.username,
                    amount: lib.to_gc_str(amount),
                    memo: memo,
                    success: true,
                    is_notification: true
                }
            };
            fobj.data = JSON.stringify(fobj.data);

            to.gc += amount;
            to.notifications.push(tobj);
            from.gc -= amount;
            from.notifications.push(fobj);
        });
    }, 1500);
    return {ok: true};
};

module.exports.xfer_gc_to = async function(context, args){
    var lib = await new Script('scripts.lib', context).call();
    if(!args || !args.to || !args.amount) return {ok: false, msg: 'Usage: accts.xfer_gc_to { to:"<user name>", amount:<number or "GC string">, memo:"<optional memo (50/1)>" }'};
    if(context.caller == args.to) return {ok: false, msg: "You can't transfer money to yourself."};
    var target = users.get(args.to);
    if(!target) return {ok: false, msg: 'User '+args.to+' does not exist.'};
    var amount = args.amount;
    if(isNaN(amount))
    {
        amount = lib.to_gc_num(amount);
    }
    else
    {
        amount = Number(amount);
    }
    if(amount === false || amount === 0 || amount % 1 != 0) return {ok: false, msg: 'You must transfer an amount as a GC string or a whole number. Examples: "1KGC" or 1000'};
    var user = users.get(context.caller);
    if(user.gc < amount) return {ok: false, msg: 'Your account balance of '+lib.to_gc_str(user.gc)+' is too low to send '+lib.to_gc_str(amount)+'.'};
    if(target.gc_max < target.gc + amount) return {ok: false, msg: "User can't store that much GC. Recipient must init their system."};
    return xfer_gc(target, user, amount, args.memo);
};