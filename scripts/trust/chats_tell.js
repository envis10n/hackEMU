var users = require('../../users').users;

function sendChat(from, to, msg){
    setTimeout(()=>{
        genKey(24, id=>{
            var fobj = {
                id: id,
                t: ts(),
                script_name: 'chats.tell',
                data:{
                    to_user: to.username,
                    msg: msg,
                    success: true,
                    is_notification: true
                }
            };
            fobj.data = JSON.stringify(fobj.data);
            var tobj = {
                id: id,
                t: ts(),
                script_name: 'chats.tell',
                data:{
                    from_user: from.username,
                    msg: msg,
                    success: true,
                    is_notification: true
                }
            };
            tobj.data = JSON.stringify(tobj.data);
            from.notifications.push(fobj);
            to.notifications.push(tobj);
        });
    }, 1500);
    return {ok:true};
};

function autos(user){
    setTimeout(()=>{
        genKey(24, id=>{
            var auto = {
                userAutos:{
                    chats:{
                        tell:{
                            to:'""',
                            msg:'""'
                        }
                    }
                },
                Count: 1
            };
            var obj = {
                id: id,
                t: ts(),
                script_name: 'autocomplete',
                data:{
                    autos: auto,
                    success: true,
                    is_notification: true
                }
            };
            obj.data = JSON.stringify(obj.data);
            user.notifications.push(obj);
        });
    }, 1500);
}

module.exports = function(context, args){
    if(!args) 
    {
        autos(users.get(context.caller));
        return 'chats.tell { to:"<username>", msg:"<message (1000/10)>" }';
    }
    else if(!args.to || !args.msg)
    {
        autos(users.get(context.caller));
        return {ok:false, msg:'chats.tell { to:"<username>", msg:"<message (1000/10)>" }'};
    }
    var target = users.get(args.to);
    if(!target)
    {
        return {
            ok: false,
            msg: 'User '+args.to+' does not exist.'
        }
    }
    else
    {
        var from = users.get(context.caller);
        return sendChat(from, target, args.msg);
    }
}