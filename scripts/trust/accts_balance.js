var users = require('../../users').users;
module.exports = async function(context, args){
    var lib = await new Script('scripts.lib', context).call();
    return lib.to_gc_str(users.get(context.caller).gc);
}