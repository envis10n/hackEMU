var users = require('../../users').users;
const {format} = require('util');
module.exports = function(context, args){
    var user = users.get(context.caller);
    return format('\n\n`C%s` (`Nemu`)\n\ntier: %d\n<stuff goes here>\n\nchannel_count: %d\n\n`Cscript space`\npublics: %d\nslots: %d\nchars: %d', context.caller, user.tier, user.channel_count, user.public_script_slots, user.script_slots, user.char_count);
}