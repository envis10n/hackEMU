var users = require('../../users').users;
module.exports = function(context, args){
    var user = users.get(context.caller);
    return {
        hardline: user.hardline,
        tutorial: "",
        breach: user.breach
    }
}