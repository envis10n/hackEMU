var users = require('../../users').users;
module.exports = function(context, args){
    var user = users.get(context.caller);
    if(!args)
    {
        if(user.hardline > 0)
        {
            return {ok: false, remaining: user.hardline, msg: ':::TRUST COMMUNICATION::: hardline is already active. Add { dc: true } to disconnect'};
        }
        else
        {
            users.hardlines.set(context.caller, true);
            return {ok: true};
        }
    }
    else if(args.activate === true)
    {
        if(users.hardlines.get(context.caller) === true)
        {
            users.hardlines.set(context.caller, users.hardline(user, 120));
            return {ok:true, remaining: 120}
        }
        else
        {
            return {ok: true};
        }
    }
    else if(args.dc === 'true')
    {
        if(user.hardline > 0)
        {
            user.hardline = 0;
            clearInterval(users.hardlines.get(context.caller));
            users.hardlines.delete(context.caller);
            return {ok: true, dc: true};
        }
        else
        {
            return null;
        }
    }
    else
    {
        return null;
    }
}