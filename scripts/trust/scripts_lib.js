module.exports = function(context, args){
    var gc_l = [
        'GC',
        'K',
        'M',
        'B',
        'T',
        'Q'
    ];
    
    var gc_d = {
        'G':0,
        'K':1,
        'M':2,
        'B':3,
        'T':4,
        'Q':5
    };
    return {
        to_gc_str: function(num){
            num = Number(num);
            var lstr = num.toLocaleString();
            lstr = lstr.split(',');
            lstr.reverse();
            var text = '';
            for(var i = lstr.length-1;i>=0;i--)
            {
                var ch = gc_l[i];
                var val = lstr[i];
                text += (val != '000'?val+ch:i==0?ch:'');
            }
            return text;
        },
        to_gc_num: function(str){
            str = str.toString();
            var text = '';
            var last = '0';
            var valid = true;
            str = str.substring(0, str.length-1);
            for(var i = 0;i<str.length;i++)
            {
                var ch = str[i];
                if(isNaN(ch))
                {
                    var r = gc_d[ch];
                    if(ch != 'C' && r == undefined)
                    {
                        valid = false;
                        break;
                    }
                    var nr = gc_d[str[i+1]];
                    if(nr != undefined)
                    {
                        text += '000'.repeat(r-nr);
                    }
                    else if(ch != 'G' && isNaN(str[i+1]))
                    {
                        text += '000';
                    }
                    else
                    {
                        nr = gc_d[str[i+4]]+1;
                        if(nr != undefined && str[i+4])
                        {
                            text += '000'.repeat(r-nr);
                        }
                    }
                }
                else
                {
                    text += ch;
                }
                last = ch;
            };
            return valid ? Number(text) : false;
        },
        security_level_names: ["NULLSEC", "LOWSEC", "MIDSEC", "HIGHSEC", "FULLSEC"],
        get_security_level_name: function(seclevel){
            if(isNaN(seclevel)) return null;
            var levels = ["NULLSEC", "LOWSEC", "MIDSEC", "HIGHSEC", "FULLSEC"];
            return levels[seclevel] ? levels[seclevel]:null;
        },
        corruption_characters: "¡¢Á¤Ã¦§¨©ª",
        rand_int:function(min, max){
            return Math.round(Math.random() * (max - min) + min);
        },
        corrupt: function(text, complexity = 1, rset = "¡¢Á¤Ã¦§¨©ª"){
            var set = rset.split('').sort(function(){
                return 0.5 - Math.random();
            });
            var tarr = text.split(' ');
            tarr.forEach(function(word, i){
                var carr = word.split('');
                tarr[i] = carr.map(function(el){
                    return (Math.random() > complexity*0.05) ? el : set[Math.round(Math.random() * ((set.length-1)-0)+0)];
                }).join('');
            });
            return tarr.join(' ');
        },
        create_rand_string: function(len){
            return genID(len, 'abcdefghijklmnopqrstuvwxyz0123456789');
        }
    }
}