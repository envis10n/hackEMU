module.exports = function(context, args){
    if(!args || !args.name) return 'Usage: scripts.get_level { name:"<scriptname>" }';
    var script = new Script(args.name, context);
    var seclevels = ["NULLSEC", "LOWSEC", "MIDSEC", "HIGHSEC", "FULLSEC"]
    if(script.valid)
    {
        return seclevels[script.script_level];
    }
    else
    {
        return {ok: false, msg: 'Script '+args.name+" doesn't exist."};
    }
}