var fiber = require('fibers');
var Future = require('fibers/future');

var db = require('../../db');

module.exports.f = function(context, ...args){
    return  db.get().collection(context.calling_script.split('.').shift()).find(...args).toArray();
};

module.exports.f1 = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).findOne(...args);
};

module.exports.u = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).updateMany(...args);
};

module.exports.u1 = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).updateOne(...args);
};

module.exports.r = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).deleteMany(...args);
};

module.exports.r1 = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).deleteOne(...args);
};

module.exports.i = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).insert(...args);
}

module.exports.i1 = function(context, ...args){
    return db.get().collection(context.calling_script.split('.').shift()).insertOne(...args);
}