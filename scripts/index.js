var scripts = require('./scripts');
const vm = require('vm');

var levels = {
    'n':0,
    'l':1,
    'm':2,
    'h':3,
    'f':4
}

function Script(script_name, context){
    const self = this;
    this.run_id = genID();
    this.script = scripts.get(script_name);
    this.valid = (this.script != undefined);
    if(this.valid && script_name.split('.')[0] == 'db') this.valid = (context.calling_script != null);
    if(!this.valid) return;
    this.script_level = this.script.script_level;
    this.call_level = this.script.call_level;
    this.called_level = 4;
    this.public = this.script.public;
    this.sector = this.script.sector;
    this.context = context;
    this.called = function(level){
        self.called_level = level;
        return self;
    };
    this.call = async function(args){
        var sconn = 'scripts_'+self.run_id;
        var scon = {
            context: self.context,
            args: args,
            Script: Script
        };
        scon[sconn] = require('./scripts');
        var vm_context = self.sector != '' ? vm.createContext(scon) : null
        try {
            return new Promise(resolver=>{
                try {
                    var good = true;
                    if(context.calling_script != null && self.call_level < self.called_level) good = false;
                    if(good)
                    {
                        var ret = vm_context != null ? vm.runInContext(`var run = `+self.script.call+`\nrun(context`+(args?', args':'')+`);`, vm_context, {lineOffset: 0, timeout: 5000, filename: 'anon'}) : self.script.call(self.context, args);
                        ret == undefined?null:ret;
                        resolver(ret);
                    }
                    else
                    {
                        resolver({ok:false,msg:"Security level guarantee failed: subscript "+script_name+" "+self.call_level+" < "+context.calling_script+" required "+self.called_level});
                    }
                } catch (e) {
                    console.log(e);
                    resolver({ok: false, msg: ':::TRUST COMMUNICATION::: Error: '+(e.message||e)});
                }
            });
        } catch (e){
        }
    }
    this.run = function(args){
        var promise = new Promise(function(resolve, reject){
            var valid = true;
            var tout = setTimeout(()=>{
                valid = false;
                resolve({
                    ok: false,
                    msg: ':::TRUST COMMUNICATION::: Error: Script execution timed out.'
                });
            }, 5000);
            self.call(args).then((ret)=>{
                if(valid)
                {
                    clearTimeout(tout);
                    if(ret == undefined) ret = null;
                    resolve(ret)
                }
            }, err=>{
                console.log(err);
                resolve({ok: false, msg: ':::TRUST COMMUNICATION::: Error: '+(err.message || err)});
            });
        });
        return promise;
    }
}

Script.prototype.toString = function(){
    return this.call();
};

Script.prototype.valueOf = function(){
    return this.call();
};

global.Script = Script;

// xfer_gc_to
// ok:true
//script_name accts.xfer direction: to, recipient: username, amount: gcstr, success:true, is_notification: true
//script_name accts.xfer direction: from, sender: username, amount: gcstr
scripts.set('scripts.lib', {call: require('./trust/scripts_lib'), script_level: 4, call_level: 4, public: true, sector: ''});

scripts.set('sys.loc', {call: require('./trust/sys_loc'), script_level: 4, call_level: 0, public: true, sector: ''});
scripts.set('sys.status', {call: require('./trust/sys_status'), script_level: 4, call_level: 3, public: true, sector: ''});
scripts.set('chats.tell', {call: require('./trust/chats_tell'), script_level: 4, call_level: 2, public: true, sector: ''});
scripts.set('sys.specs', {call: require('./trust/sys_specs'), script_level: 4, call_level: 3, public: true, sector: ''});
scripts.set('accts.balance', {call: require('./trust/accts_balance'), script_level: 4, call_level: 4, public: true, sector: ''});
scripts.set('kernel.hardline', {call: require('./trust/kernel_hardline'), script_level: 4, call_level: -1, public: true, sector: ''});
scripts.set('accts.xfer_gc_to', {call: require('./trust/accts_xfer').xfer_gc_to, script_level: 4, call_level: 2, public: true, sector: ''});
scripts.set('scripts.get_level', {call: require('./trust/scripts_get_level'), script_level: 4, call_level: 4, public: true, sector: ''});
// DB
scripts.set('db.f', {call: require('./db/db').f, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.f1', {call: require('./db/db').f1, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.u', {call: require('./db/db').u, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.u1', {call: require('./db/db').u1, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.r', {call: require('./db/db').r, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.r1', {call: require('./db/db').r1, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.i', {call: require('./db/db').i, script_level:4, call_level: 4, public:true, sector: ''});
scripts.set('db.i1', {call: require('./db/db').i1, script_level:4, call_level: 4, public:true, sector: ''});