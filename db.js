const MongoClient = require('mongodb').MongoClient;
const EventHandler = require('events');

module.exports = new EventHandler();

var client;

module.exports.start = function(){
    MongoClient.connect('mongodb://localhost:27017/hackemu', { useNewUrlParser: true }, (err, cli)=>{
        if(err) throw err;
        client = cli;
        console.log('DB connected.');
        module.exports.emit('ready');
    });
}

module.exports.get = function(){
    return client?client.db():undefined;
}