const net = require('tls');
const https = require('https');
const express = require('express');
const session = require('express-session');
const fs = require('fs');
const EventHandler = require('events');
const bodyParser = require('body-parser');
const shortid = require('shortid');
const bcrypt = require('bcryptjs');
const Entities = require('html-entities').AllHtmlEntities;

const vm = require('vm');

Array.prototype.shuffle = function (){
    for (let i = this.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
}

Math.random_int = function(max = 1, min = 0){
    return Math.round(Math.random() * (max - min) + min);
}

Math.random_float = function(max = 1, min = 0){
    return Math.random() * (max - min) + min;
}

var locend = 'extern pub p access public entry info external pubinfo out pub_info'.split(' ');

var runs = new Map();

const html = new Entities();

global.genID = function(length = 24, chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'){
    chars = chars.split('');
    chars.shuffle();
    var id = '';
    for(var i = 0;i<length;i++)
    {
        id += chars[Math.random_int(chars.length-1)];
        chars.shuffle();
    }
    return id;
}

require('./scripts/index');
var scripts = require('./scripts/scripts');

global.genKey = function(length, cb, input = ''){
    var t = input;
    t += shortid.generate().replace(/[^a-zA-Z0-9]/g, '');
    if(t.length >= length)
    {
        cb(t.substr(0, length));
    }
    else
    {
        genKey(length, cb, t);
    }
}

function genLoc(username, cb){
    var end = locend[Math.round(Math.random() * locend.length)];
    genKey(6, id=>{
        id = id.toLowerCase();
        cb(username+'.'+end+'_'+id);
    });
}

global.ts = function(){
    return Math.floor(Date.now()/1000);
}

var db = require('./db');

var userlist = require('./users');
var users = userlist.users;
var accounts = userlist.accounts;
var channels = userlist.channels;

var events = new EventHandler();

module.exports = events;

var ssloptions = {
    pfx: fs.readFileSync('./_certificates/hackmud.pfx'),
    passphrase: "1234"
};

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.get('*', (req, res)=>{
    res.destroy();
});

app.use((req, res, next)=>{
    if(req.originalUrl != '/notifications.json')
        console.log('::['+req.originalUrl+']::\n', req.body);
    next();
});

app.post('/accounts/sign_in_steam_session.json', (req, res)=>{
    genKey(20, (atoken)=>{
        var user = {
            uid: shortid.generate()+shortid.generate()+shortid.generate()+shortid.generate(),
            account_token: atoken
        };
        accounts.set(atoken, user);
        res.json({authentication_token: atoken});
    });
});

app.post('/check_username.json', (req,res)=>{
    //TODO: handle username checks for REAL.
    //NEW USER RESPONSE {"available":true,"msg":"Confirm creating `Cenvisderp` with `Ccreate_user` envisderp"}
    //NEW USER CREATE RESPONSE {"available":true,"msg":"4 of 7 users created. Use `Cstore_list` to purchase more slots."}
    //run.json {"success":true,"run_id":"5b0eb590bed7373ea6e807e4","corruption_level":0}
    if(!req.body.username)
        res.json({available: false, msg: "Enter a username."});
    else
    {
        var account = accounts.get(req.body.account_token);
        if(!account)
        {
            account = {
                uid: shortid.generate()+shortid.generate()+shortid.generate()+shortid.generate(),
                account_token: req.body.account_token
            }
            accounts.set(req.body.account_token, account);
        }
        if(!account.username)
        {
            account.username = req.body.username;
            res.json({available: false, msg: "Enter password."});
        }
        else if(!account.password)
        {
            var pass = req.body.username;
            var user = users.get(account.username);
            if(!user)
            {
                bcrypt.genSalt(13, (err, salt)=>{
                    bcrypt.hash(pass, salt, (err, hash)=>{
                        genLoc(account.username, loc=>{
                            var usr = {
                                username: account.username,
                                password: hash,
                                notifications: [],
                                breach: false,
                                hardline: 0,
                                tier: 1,
                                gc: 100000,
                                gc_limit: 1000000000000,
                                script_slots: 100,
                                public_script_slots: 100,
                                char_count: 1000000,
                                channel_count: 100000,
                                loc: loc
                            };
                            delete pass;
                            db.get().collection('users').insertOne(usr, (err, result)=>{
                                users.set(usr.username, usr);
                                account.password = true;
                                res.json({available: false, msg: "`2New user created`\n\nWelcome.\n\nRe-enter username."});
                                setTimeout(()=>{

                                });
                            });
                        });
                    });
                });
            }
            else
            {
                bcrypt.compare(pass, user.password, (err, valid)=>{
                    if(!valid)
                    {
                        account.username = undefined;
                        account.password = undefined;
                        res.json({available: false, msg: "Access denied.\n\nEnter a username."});
                    }
                    else
                    {
                        account.password = true;
                        res.json({available: false, msg: "`2Success`\n\nRe-enter username."});
                    }
                });
            }
        }
        else
        {
            if(req.body.username = account.username)
            {
                res.json({available:true});
            }
            else
            {
                res.json({available: false, msg: "Incorrect username associated with this session.\n\nRe-enter username."});
            }
            account.username = undefined;
            account.password = undefined;
        }
    }
});

app.post('/run.json', (req, res)=>{
    var runid = genID();
    res.json({success: true, run_id: runid, corruption_level: 0});
    var context = {};
    var user = users.get(req.body.username);
    context.caller = user.username;
    context.calling_script = null;
    context.is_scriptor = false;
    var dargs = decodeURIComponent(req.body.args);
    var args = req.body.args ? JSON.parse(dargs) : undefined;
    if(args)
    {
        Object.keys(args).forEach(key=>{
            var d = args[key];
            if(typeof(d) == 'object')
            {
                if(d['__scriptor'])
                {
                    var script = new Script(d['__scriptor'], {caller: context.caller, calling_script: null, is_scriptor: true});
                    if(script.valid && script.call_level != -1)
                    {
                        args[key] = script;
                    }
                    else
                    {
                        args[key] = undefined;
                    }
                }
            }
        });
    }
    var obj = {
        id: runid,
        t: ts(),
        script_name: req.body.script_name,
        data: {
            success: true,
            corruption_level: 0
        }
    };
    var script = new Script(req.body.script_name, context); // Fix for hardline from shell, but keep it locked from scriptor access.
    var canrun = script.valid ? (script.public?true:req.body.script_name.split('.')[0] == req.body.username):false;
    if(canrun)
    {
        runs.set(runid, {script_name: req.body.script_name, context: context});
        try {
            script.run(args).then((retval)=>{
                if(runs.get(runid))
                {     
                    obj.data.retval = retval;
                    obj.data = JSON.stringify(obj.data);
                    user.notifications.push(obj);
                    runs.delete(runid);
                }
            }, err=>{
                throw err;
            });
        } catch (e) {
            console.log(e);
            obj.data.retval = {
                ok: false,
                msg: ':::TRUST COMMUNICATION::: Error: '+e.message
            };
            if(runs.get(runid))
            {
                obj.data = JSON.stringify(obj.data);
                user.notifications.push(obj);
            }
        }
    }
    else
    {
        obj.data = {
            success: false,
            retval: ':::TRUST COMMUNICATION::: PARSE ERROR '+req.body.script_name+": script doesn't exist",
            corruption_level: 0
        };
        obj.data = JSON.stringify(obj.data);
        user.notifications.push(obj);
    }
});

app.post('/notifications.json', (req, res)=>{
    var user = users.get(req.body.username);
    if(!user)
    {
        res.json([]);
    }
    else
    {
        res.json(user.notifications);
        user.notifications = [];
    }
});

app.post('/client_data.json', (req,res)=>{
    //TODO: store/return client data for user
    res.json({});
});

var levels = {
    'n':0,
    'l':1,
    'm':2,
    'h':3,
    'f':4
}

var dops = {
    'f':'findMany',
    'f1':'findOne',
    'u':'updateMany',
    'u1':'updateOne',
    'r':'deleteMany',
    'r1':'deleteOne',
    'i':'insertMany',
    'i1':'insertOne'
}

app.post('/update.json', (req, res)=>{
    //TODO: Handle uploads, delets, etc.
    var dobj = req.body;
    var script_name = dobj.username+'.'+dobj.script_name;
    var user = users.get(dobj.username);
    if(!dobj.script_code)
    {
        scripts.delete(script_name);
        db.get().collection('user_scripts').deleteOne({script_name: script_name});
        res.json({success:true,msg:":::TRUST COMMUNICATION::: Script successfully deleted."});
    }
    else
    {
        try {
            var scrReg = new RegExp(/(\#[nlmhf]s.[a-zA-Z0-9_]+.[a-zA-Z0-9_]+)\(([\s\S]*)\)/);
            var dbReg = new RegExp(/\#db.([furi]1?)\((\{[\s\S]*\})\)/);
            var nReg = new RegExp(/new Script/, 'g');
            var code = decodeURIComponent(dobj.script_code);
            var script_len = code.replace(/\/\/[^\n\r]*/gi,'').replace(/\/\*[\s\S]*\*\//gi, '').replace(/[\s\n\r\t]/gi, '').length;
            var codelines = code.split('\n');
            var level = 4;
            for(var i = 0; i < codelines.length;i++)
            {
                var cod = codelines[i];
                var nmatch = nReg.exec(cod);
                if(nmatch)
                {
                    throw new Error("Script is undefined.");
                }
                var scrmatch = scrReg.exec(cod);
                if(scrmatch)
                {
                    var scr = scrmatch[1].split('.');
                    var args = scrmatch[2];
                    var lvl = levels[scr[0][1]];
                    level = lvl < level ? lvl:level;
                    var name = scr.slice(1).join('.');
                    var script = 'new Script("'+name+'", {caller: context.caller, calling_script: "'+script_name+'", is_scriptor: false}).called('+lvl+').call('+args+')';
                    codelines[i] = cod.replace(scrmatch[0], script);
                }
                var dbmatch = dbReg.exec(cod);
                if(dbmatch)
                {
                    var dbop = dbmatch[1];
                    var args = dbmatch[2];
                    if(args == '') args = '{}';
                    var dbc = 'new Script("db.'+dbop+'", {caller: context.caller, calling_script: "'+script_name+'", is_scriptor: false}).call('+args+')';
                    codelines[i] = cod.replace(dbmatch[0], dbc);
                }
            }
            code = codelines.join('\n');
            console.log(code);
            var compiled = new vm.Script('var run = '+code, {filename: 'anon', timeout: 5000});
            var script = scripts.get(script_name);
            var obj = {};
            obj.call = code;
            obj.script_level = level;
            obj.call_level = level;
            obj.public = dobj.script_is_public ? true: (script?(script.public?true:false):false);
            obj.sector = obj.public ? 'PUBLIC':'PRIVATE';
            scripts.set(script_name, obj);
            db.get().collection('user_scripts').updateOne({script_name: script_name},{$set:{script_name: script_name, data: obj, raw: dobj.script_code}}, {upsert: true});
            res.json({success:true,security_level:obj.script_level,length:script_len+'/'+user.char_count,is_public: obj.public});
        } catch(e){
            console.log(e);
            res.json({retval:e.message,success:false});
        }
    }
});

app.use('/download.json', (req, res)=>{
    var obj = {};
    var script_name = req.body.username+'.'+req.body.script_name;
    db.get().collection('user_scripts').find({script_name: script_name}).toArray((err, docs)=>{
        if(err) throw err;
        var script = docs[0];
        obj.code = script == undefined ? undefined : script.raw;
        res.json(obj);
    });
});

app.use('*', (req, res)=>{

});

var server = https.createServer(ssloptions, app);

db.start();

db.on('ready', ()=>{
    db.get().collection('users').find({}).toArray((err, docs)=>{
        if(err) throw err;
        docs.forEach(user=>{
            users.set(user.username, user);
            if(user.hardline > 0) users.hardline(user, user.hardline);
        });
        db.get().collection('accounts').find({}).toArray((err, doc)=>{
            if(err) throw err;
            doc.forEach(acc=>{
                accounts.set(acc.account_token, acc);
            });
            setInterval(()=>{
                users.forEach(user=>{
                    db.get().collection('users').updateOne({username: user.username},{$set:user});
                });
            });
            db.get().collection('user_scripts').find({}).toArray((err, uscripts)=>{
                if(err) throw err;
                uscripts.forEach(uscript=>{
                    scripts.set(uscript.script_name, uscript.data);
                });
                server.listen(443, ()=>{
                    console.log('hackEMU online.');
                });
            });
        });
    });
});